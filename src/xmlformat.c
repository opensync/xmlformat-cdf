/*
 * xmlformat - registration of xml object formats 
 * Copyright (C) 2004-2005  Armin Bauer <armin.bauer@opensync.org>
 * Copyright (C) 2006  Daniel Friedrich <daniel.friedrich@opensync.org>
 * Copyright (C) 2007  Daniel Gollub <gollub@b1-systems.de>
 * Copyright (C) 2007  Jerry Yu <jijun.yu@sun.com>
 * Copyright (C) 2007  Christopher Stender <cstender@suse.de>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

#include "xmlformat.h"

osync_bool destroy_xmlformat(char *input, unsigned int inpsize, void *userdata, OSyncError **error)
{
	osync_xmlformat_unref((OSyncXMLFormat *)input);
	return TRUE;
}

static osync_bool duplicate_xmlformat(const char *uid, const char *input, unsigned int insize, char **newuid, char **output, unsigned int *outsize, osync_bool *dirty, void *userdata, OSyncError **error)
{
	osync_trace(TRACE_ENTRY, "%s(%s, %p, %i, %p, %p, %p, %p, %p)", __func__, uid, input, insize, newuid, output, outsize, dirty, error);

	*dirty = TRUE;
	*newuid = g_strdup_printf ("%s-dupe", uid);
	if (!*newuid)
		goto error;

	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}


osync_bool copy_xmlformat(const char *input, unsigned int inpsize, char **output, unsigned int *outpsize, void *userdata, OSyncError **error)
{
	OSyncXMLFormat *xmlformat = NULL;
	osync_trace(TRACE_ENTRY, "%s(%p, %i, %p, %p, %p)", __func__, input, inpsize, output, outpsize, error);

	if (!osync_xmlformat_copy((OSyncXMLFormat *) input, &xmlformat, error) || !xmlformat) {
		osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
		return FALSE;
	}

	/* TODO: Try to avoid sorting */
	if (!osync_xmlformat_sort(xmlformat, error)) {
		osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
		return FALSE;
	}

	*output = (char *) xmlformat;
	*outpsize = osync_xmlformat_size();

	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;
}
char *print_xmlformat(const char *data, unsigned int size, void *userdata, OSyncError **error)
{
	char *buffer;
	unsigned int i;

	osync_assert(data);
	osync_assert(size > 0);

	if(!osync_xmlformat_assemble((OSyncXMLFormat *)data, &buffer, &i, error))
		goto error;

	return buffer;

error:
	return NULL;
}

osync_bool marshal_xmlformat(const char *input, unsigned int inpsize, OSyncMarshal *marshal, void *userdata, OSyncError **error)
{
	char *buffer;
	unsigned int size;

	if (!osync_xmlformat_assemble((OSyncXMLFormat *)input, &buffer, &size, error))
		goto error;

	if (!osync_marshal_write_buffer(marshal, buffer, (int)size, error))
		goto error;

	g_free(buffer);

	return TRUE;

error:
	return FALSE;
}

osync_bool demarshal_xmlformat(OSyncMarshal *marshal, char **output, unsigned int *outpsize, void *userdata, OSyncError **error)
{
	void *buffer = NULL;
	unsigned int size = 0;
	OSyncXMLFormat *xmlformat = NULL;
	if (!osync_marshal_read_buffer(marshal, &buffer, &size, error))
		goto error;

	xmlformat = osync_xmlformat_parse((char *)buffer, size, error);
	if (!xmlformat)
		goto error;

	/* FIXME: Avoid sorting in demarshal call!
	 *        Demarshal the entire XMLFormat struct, including the sorte
	 *        attribute to keep this information.
	 */
	if (!osync_xmlformat_sort(xmlformat, error))
		goto error;

	g_free(buffer);

	*output = (char*)xmlformat;
	*outpsize = osync_xmlformat_size();
	return TRUE;

error:
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}


OSyncConvCmpResult compare_contact(const char *leftdata, unsigned int leftsize, const char *rightdata, unsigned int rightsize, void *userdata, OSyncError **error)
{
	char* keys_content[] =	{"Content", NULL};
	char* keys_name[] = {"FirstName", "LastName", NULL};
	OSyncXMLPoints points[] = {
		{"EMail",           10, keys_content},
		{"FormattedName",   -1, keys_content},
		{"Name",            90, keys_name},
		{"Revision",        -1, keys_content},
		{"Telephone",       10, keys_content},
		{"Uid",             -1, keys_content},
		{NULL}
	};
	OSyncConvCmpResult ret;

	osync_trace(TRACE_ENTRY, "%s(%p, %i, %p, %i)", __func__, leftdata, leftsize, rightdata, rightsize);

	ret = xmlformat_compare((OSyncXMLFormat *)leftdata, (OSyncXMLFormat *)rightdata, points, 0, 100);

	osync_trace(TRACE_EXIT, "%s: %i", __func__, ret);
	return ret;
}

static osync_bool create_contact(char **data, unsigned int *size, void *userdata, OSyncError **error)
{
	*data = (char *)osync_xmlformat_new("contact", error);
	if (!*data)
		goto error;

	return TRUE;
error:
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

static OSyncConvCmpResult compare_event(const char *leftdata, unsigned int leftsize, const char *rightdata, unsigned int rightsize, void *userdata, OSyncError **error)
{
	char* keys_content[] =	{"Content", NULL};
	OSyncXMLPoints points[] = {
		{"Alarm",               -1, keys_content}, // Not implemented
		{"Created",             -1, keys_content}, // It changes ... weird . Digg further 
		{"DateCalendarCreated", -1, keys_content},
		{"DateEnd",             10, keys_content},
		{"DateStarted",         10, keys_content},
		{"LastModified",        -1, keys_content},// fixme
		{"Method",              -1, keys_content},// fixme
		{"ProductID",           -1, keys_content},
		{"Status",              -1, keys_content},
		{"Summary",             90, keys_content},
		{"Uid",                 -1, keys_content},
		{NULL}
	};
	OSyncConvCmpResult ret;

	osync_trace(TRACE_ENTRY, "%s(%p, %p)", __func__, leftdata, rightdata);

	ret = xmlformat_compare((OSyncXMLFormat *)leftdata, (OSyncXMLFormat *)rightdata, points, 0, 100);

	osync_trace(TRACE_EXIT, "%s: %i", __func__, ret);
	return ret;
}

osync_bool create_event(char **data, unsigned int *size, void *userdata, OSyncError **error)
{
	*data = (char *)osync_xmlformat_new("event", error);
	if (!*data)
		goto error;

	return TRUE;

error:
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

static OSyncConvCmpResult compare_todo(const char *leftdata, unsigned int leftsize, const char *rightdata, unsigned int rightsize, void *userdata, OSyncError **error)
{
	char* keys_content[] =	{"Content", NULL};
	OSyncXMLPoints points[] = {
		{"DateCalendarCreated",     -1,  keys_content},// Not in vtodo10
		{"DateStarted",             10,  keys_content},
		{"Due",                     10,  keys_content},
		{"Method",                  -1,  keys_content}, // Not in vtodo10
		{"PercentComplete",         -1,  keys_content}, // Not in vtodo10
		{"ProductID",               -1,  keys_content},
		{"Summary",                 90,  keys_content},
		{"Timezone",                -1,  keys_content}, // Not in vtodo10
		{"TimezoneComponent",       -1,  keys_content}, // Not in vtodo10
		{"TimezoneRule",            -1,  keys_content}, // Not in vtodo10
		{"Uid",                     -1,  keys_content},
		{NULL}
	};
	OSyncConvCmpResult ret;

	osync_trace(TRACE_ENTRY, "%s(%p, %p)", __func__, leftdata, rightdata);

	ret = xmlformat_compare((OSyncXMLFormat *)leftdata, (OSyncXMLFormat *)rightdata, points, 0, 100);

	osync_trace(TRACE_EXIT, "%s: %i", __func__, ret);
	return ret;
}

static osync_bool create_todo(char **data, unsigned int *size, void *userdata, OSyncError **error)
{
	*data = (char *)osync_xmlformat_new("todo", error);
	if (!*data)
		goto error;

	return TRUE;

error:
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

static OSyncConvCmpResult compare_note(const char *leftdata, unsigned int leftsize, const char *rightdata, unsigned int rightsize, void *userdata, OSyncError **error)
{
	char* keys_content[] =	{"Content", NULL};
	OSyncXMLPoints points[] = {
		{"Class",                 -1, keys_content},// fixme
		{"Created",               -1, keys_content},// fixme
		{"DateCalendarCreated",   -1, keys_content},
		{"Description",           90, keys_content},
		{"LastModified",          -1, keys_content},// fixme
		{"Method",                -1, keys_content},
		{"ProductID",             -1, keys_content},
		{"Summary",               90, keys_content},
		{"Uid",                   -1, keys_content},
		{NULL}
	};
	OSyncConvCmpResult ret;
	osync_trace(TRACE_ENTRY, "%s(%p, %p)", __func__, leftdata, rightdata);


	ret = xmlformat_compare((OSyncXMLFormat *)leftdata, (OSyncXMLFormat *)rightdata, points, 0, 100);

	osync_trace(TRACE_EXIT, "%s: %i", __func__, ret);
	return ret;
}

static osync_bool create_note(char **data, unsigned int *size, void *userdata, OSyncError **error)
{
	*data = (char *)osync_xmlformat_new("note", error);
	if (!*data)
		goto error;

	return TRUE;

error:
	osync_trace(TRACE_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

static time_t get_revision(const char *data, unsigned int size, const char *attribute, OSyncError **error)
{
	OSyncXMLFieldList *fieldlist = NULL;
	int length = 0;
	OSyncXMLField *xmlfield = NULL;
	const char *revision = NULL;
	time_t time;

	osync_trace(TRACE_ENTRY, "%s(%p, %u, %p, %p)", __func__, data, size, attribute, error);

	/* TODO: Avoid sorting somehow */
	if (!osync_xmlformat_sort((OSyncXMLFormat *)data, error))
		goto error;

	fieldlist = osync_xmlformat_search_field((OSyncXMLFormat *)data, attribute, error, NULL);
	if (!fieldlist)
		goto error;

	length = osync_xmlfieldlist_get_length(fieldlist);
	if (length != 1) {
		osync_xmlfieldlist_free(fieldlist);
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Unable to find the revision.");
		goto error;
	}

	xmlfield = osync_xmlfieldlist_item(fieldlist, 0);
	osync_xmlfieldlist_free(fieldlist);

	revision = osync_xmlfield_get_nth_key_value(xmlfield, 0);
	osync_trace(TRACE_INTERNAL, "About to convert string %s", revision);
	//time_t time = vformat_time_to_unix(revision);
	time = osync_time_vtime2unix(revision, 0, error);

	osync_trace(TRACE_EXIT, "%s: %li", __func__, time);
	return time;

 error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return -1;
}

static time_t get_contact_revision(const char *data, unsigned int size, void *userdata, OSyncError **error)
{
	return get_revision(data, size, "Revision", error);
}

static time_t get_event_revision(const char *data, unsigned int size, void *userdata, OSyncError **error)
{
	return get_revision(data, size, "LastModified", error);
}

static time_t get_note_revision(const char *data, unsigned int size, void *userdata, OSyncError **error)
{
	return get_revision(data, size, "LastModified", error);
}

static time_t get_todo_revision(const char *data, unsigned int size, void *userdata, OSyncError **error)
{
	return get_revision(data, size, "LastModified", error);
}

osync_bool validate_xmlformat(const char *data, unsigned int size, void *user_data, OSyncError **error)
{
	osync_bool ret;
	OSyncXMLFormat *xmlformat = (OSyncXMLFormat *) data;
	XMLFormat *xmlformat_data = user_data;
	osync_assert(xmlformat);

	osync_trace(TRACE_ENTRY, "%s(%p, %u, %p, %p)", __func__, data, size, user_data, error);

	ret = osync_xmlformat_schema_validate(xmlformat_data->xmlformat_schema, xmlformat, error);

	osync_trace(TRACE_EXIT, "%s: %s", __func__, ret ? "TRUE" : "FALSE");
	return ret;
}

static osync_bool finalize(void *userdata, OSyncError **error)
{
	XMLFormat *xmlformat_data = (XMLFormat *) userdata;

	if (!userdata)
		return TRUE; 

	if (xmlformat_data->xmlformat_schema)
		osync_xmlformat_schema_unref(xmlformat_data->xmlformat_schema);

	osync_free(xmlformat_data);

	return TRUE;
}

static void *initialize(const char *objtype, OSyncError **error)
{
	XMLFormat *userdata = NULL;
	osync_trace(TRACE_ENTRY, "%s(%s, %p)", __func__, objtype, error);

	userdata = osync_try_malloc0(sizeof(XMLFormat), error);
	if (!userdata)
		goto error;

	userdata->xmlformat_schema = osync_xmlformat_schema_new(objtype, error);
	if (!userdata->xmlformat_schema)
		goto error_and_free;

	osync_trace(TRACE_EXIT, "%s: %p", __func__, userdata);
	return (void *) userdata;

error_and_free:
	finalize(userdata, NULL);
error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return NULL;
}

static void *initialize_contact(OSyncError **error)
{
	return initialize("contact", error);
}

static void *initialize_event(OSyncError **error)
{
	return initialize("event", error);
}

static void *initialize_todo(OSyncError **error)
{
	return initialize("todo", error);
}

static void *initialize_note(OSyncError **error)
{
	return initialize("note", error);
}

static osync_bool register_merger(OSyncFormatEnv *env, const char *objformat, OSyncError **error)
{
        /* register xmlformat merger */
        OSyncMerger *merger = osync_merger_new(objformat, "xmlformat", error);
        if (!merger)
                goto error;

	osync_merger_set_merge_func(merger, merge_xmlformat);
	osync_merger_set_demerge_func(merger, demerge_xmlformat);


        if (!osync_format_env_register_merger(env, merger, error))
		goto error;

        osync_merger_unref(merger);

	return TRUE;

error:
	return FALSE;
}

osync_bool get_format_info(OSyncFormatEnv *env)
{
	OSyncError *error = NULL;
	OSyncObjFormat *format = NULL;

	/* register xmlformat-contact */
	format = osync_objformat_new("xmlformat-contact", "contact", &error);
	if (!format)
		goto error;

	osync_objformat_set_initialize_func(format, initialize_contact);
	osync_objformat_set_finalize_func(format, finalize);

	osync_objformat_set_compare_func(format, compare_contact);
	osync_objformat_set_destroy_func(format, destroy_xmlformat);
	osync_objformat_set_duplicate_func(format, duplicate_xmlformat);
	osync_objformat_set_print_func(format, print_xmlformat);
	osync_objformat_set_copy_func(format, copy_xmlformat);
	osync_objformat_set_create_func(format, create_contact);
	osync_objformat_set_validate_func(format, validate_xmlformat);

	osync_objformat_set_revision_func(format, get_contact_revision);

	osync_objformat_set_marshal_func(format, marshal_xmlformat);
	osync_objformat_set_demarshal_func(format, demarshal_xmlformat);


	if (!osync_format_env_register_objformat(env, format, &error))
		goto error;

	osync_objformat_unref(format);

	if (!register_merger(env, "xmlformat-contact", &error))
		goto error;

	/* register xmlformat-event */
	format = osync_objformat_new("xmlformat-event", "event", &error);
	if (!format)
		goto error;

	osync_objformat_set_initialize_func(format, initialize_event);
	osync_objformat_set_finalize_func(format, finalize);

	osync_objformat_set_compare_func(format, compare_event);
	osync_objformat_set_destroy_func(format, destroy_xmlformat);
	osync_objformat_set_duplicate_func(format, duplicate_xmlformat);
	osync_objformat_set_print_func(format, print_xmlformat);
	osync_objformat_set_copy_func(format, copy_xmlformat);
	osync_objformat_set_create_func(format, create_event);
	osync_objformat_set_validate_func(format, validate_xmlformat);

	osync_objformat_set_revision_func(format, get_event_revision);

	osync_objformat_set_marshal_func(format, marshal_xmlformat);
	osync_objformat_set_demarshal_func(format, demarshal_xmlformat);

	if (!osync_format_env_register_objformat(env, format, &error))
		goto error;

	osync_objformat_unref(format);

	if (!register_merger(env, "xmlformat-event", &error))
		goto error;

	/* register xmlformat-todo */
	format = osync_objformat_new("xmlformat-todo", "todo", &error);
	if (!format)
		goto error;

	osync_objformat_set_initialize_func(format, initialize_todo);
	osync_objformat_set_finalize_func(format, finalize);

	osync_objformat_set_compare_func(format, compare_todo);
	osync_objformat_set_destroy_func(format, destroy_xmlformat);
	osync_objformat_set_duplicate_func(format, duplicate_xmlformat);
	osync_objformat_set_print_func(format, print_xmlformat);
	osync_objformat_set_copy_func(format, copy_xmlformat);
	osync_objformat_set_create_func(format, create_todo);
	osync_objformat_set_validate_func(format, validate_xmlformat);

	osync_objformat_set_revision_func(format, get_todo_revision);

	osync_objformat_set_marshal_func(format, marshal_xmlformat);
	osync_objformat_set_demarshal_func(format, demarshal_xmlformat);

	if (!osync_format_env_register_objformat(env, format, &error))
		goto error;

	osync_objformat_unref(format);

	if (!register_merger(env, "xmlformat-todo", &error))
		goto error;

	/* register xmlformat-note */
	format = osync_objformat_new("xmlformat-note", "note", &error);
	if (!format)
		goto error;

	osync_objformat_set_initialize_func(format, initialize_note);
	osync_objformat_set_finalize_func(format, finalize);

	osync_objformat_set_compare_func(format, compare_note);
	osync_objformat_set_destroy_func(format, destroy_xmlformat);
	osync_objformat_set_duplicate_func(format, duplicate_xmlformat);
	osync_objformat_set_print_func(format, print_xmlformat);
	osync_objformat_set_copy_func(format, copy_xmlformat);
	osync_objformat_set_create_func(format, create_note);
	osync_objformat_set_validate_func(format, validate_xmlformat);

	osync_objformat_set_revision_func(format, get_note_revision);

	osync_objformat_set_marshal_func(format, marshal_xmlformat);
	osync_objformat_set_demarshal_func(format, demarshal_xmlformat);

	if (!osync_format_env_register_objformat(env, format, &error))
		goto error;

	osync_objformat_unref(format);

	if (!register_merger(env, "xmlformat-note", &error))
		goto error;

	return TRUE;

error:
	osync_trace(TRACE_ERROR, "Unable to register format xmlformat: %s", osync_error_print(&error));
	osync_error_unref(&error);
	return FALSE;
}

int get_version(void)
{
	return 1;
}

