/*
 * libopensync - A synchronization framework
 * Copyright (C) 2006  NetNix Finland Ltd <netnix@netnix.fi>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * Author: Daniel Friedrich <daniel.friedrich@opensync.org>
 * 
 */
 
#include "xmlformat.h"

#include <opensync/opensync-capabilities.h>

osync_bool merge_xmlformat(char **buf, unsigned int *size, const char *entirebuf, unsigned int entiresize, OSyncCapabilities *caps, void *userdata, OSyncError **error)
{
	OSyncXMLField *old_cur, *new_cur, *tmp;
	OSyncCapability *cap_cur;
	OSyncXMLFormat *xmlformat, *entire;
	int ret;
	const char *objtype;
	
	osync_trace(TRACE_ENTRY, "%s(%p, %p:%u, %p, %u, %p, %p, %p)", __func__, buf, size, *size, entirebuf, entiresize, caps, userdata, error);

	osync_assert(osync_xmlformat_size() == *size);
	osync_assert(osync_xmlformat_size() == entiresize);


	xmlformat = (OSyncXMLFormat *) *buf; 
	entire = (OSyncXMLFormat *) entirebuf; 

	objtype = osync_xmlformat_get_objtype(xmlformat);
	
        OSyncCapabilitiesObjType *capsobjtype = osync_capabilities_get_objtype(caps, objtype);
        OSyncList *caplist = osync_capabilities_objtype_get_caps(capsobjtype);
	cap_cur = (OSyncCapability *) caplist->data;
	if(!cap_cur)
		goto error;
	 	
	new_cur = osync_xmlformat_get_first_field(xmlformat);
	old_cur = osync_xmlformat_get_first_field(entire);
	while(old_cur != NULL)
		{		 	
			ret = strcmp(osync_xmlfield_get_name(new_cur), osync_xmlfield_get_name(old_cur));
			if(ret < 0) {
				if(osync_xmlfield_get_next(new_cur) != NULL) {
					new_cur = osync_xmlfield_get_next(new_cur);
					continue;
				}
			}
		 
			/* This should be always NULL reg. #820 */
			osync_assert(cap_cur);

			ret = strcmp(osync_capability_get_name(cap_cur), osync_xmlfield_get_name(old_cur));
			if(ret == 0)
				{
					/* 
					 * now we have to merge the key/value pairs (second level)
					 * we see the second level as sorted and with the same fields (exception the last key)
					 * KEY(new)		Capabilities		KEY(old)
					 * KEY1				KEY1				KEY1
					 * KEY2(empty)		KEY3				KEY2
					 * KEY2(empty)							KEY2
					 * KEY3									KEY3
					 * 										KEY4
					 * 										KEY4
					 */
					if( osync_capability_get_childs(cap_cur) &&
							!strcmp(osync_capability_get_name(cap_cur), osync_xmlfield_get_name(new_cur))) 
						{
							OSyncXMLField *new_tmp;
							OSyncList *cap_tmp, *childs = osync_capability_get_childs(cap_cur);
							OSyncXMLField *new_node = osync_xmlfield_get_child(new_cur);
							OSyncXMLField *old_node = osync_xmlfield_get_child(old_cur);
			
							while(old_node) {
								GSList *list, *tmp;
								int i, size;
								const char *curkeyname;
					
								size=0;
								curkeyname = osync_xmlfield_get_name(old_node);
								list = NULL;
								do {
									list = g_slist_prepend(list, old_node);
									size++;
									old_node = osync_xmlfield_get_next(old_node);
									if(old_node == NULL)
										break;
									i = strcmp(osync_xmlfield_get_name(old_node), curkeyname);
								} while(i == 0);

								/* search for the curkeyname in the capabilities */
								for(cap_tmp = childs; cap_tmp != NULL; cap_tmp = cap_tmp->next) {
									if(!strcmp(osync_capability_get_name((OSyncCapability *) cap_tmp->data), curkeyname)) {
										childs = cap_tmp;
										break;
									}
								}

								if(cap_tmp) { 
									/* curkeyname was found in the capibilities */
									/* we have to set the new_node ptr to the right position */
									for(; new_node && size > 0; size--) {
										new_node = osync_xmlfield_get_next(new_node);
									}
								}else{
									/* curkeyname was _not_ found in the capabilities */
									/* link all key/value pairs with the key curkeyname to the the new_node */
						
									list = g_slist_reverse(list);

									if(new_node == NULL) {
										for(tmp=list; tmp != NULL; tmp = g_slist_next(tmp)) {
											osync_xmlfield_unlink(tmp->data);
										}
									}else{
										for(tmp=list; tmp != NULL; tmp = g_slist_next(tmp)) {
											osync_xmlfield_unlink(tmp->data);

											osync_xmlfield_adopt_xmlfield_before_field(new_node, tmp->data);
										}
							
										do{
											new_tmp = new_node;
											new_node = osync_xmlfield_get_next(new_node);
											osync_xmlfield_delete(new_tmp);
											size--;
										}while(size > 0 && new_node);
							
									}
								}
								g_slist_free(list);
							}
						}
					old_cur = osync_xmlfield_get_next(old_cur);
					continue;		 		
				}
			else if(ret < 0)
				{
                                        caplist = caplist->next;
					cap_cur = caplist->data;
					continue;
				}
			else if(ret > 0)
				{
					tmp = old_cur;
					old_cur = osync_xmlfield_get_next(old_cur);
					osync_xmlfield_adopt_xmlfield_before_field(new_cur, tmp);
					continue;
				}
			g_assert_not_reached();
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Unexpected error during XMLFormat merging");
			goto error;
		}

	/* FIXME: Merger is broken! 
		 After merging xmlformat is not sorted. Sorting is very expensive!
		 Avoid it! Ticket: #754 */
	osync_assert(osync_xmlformat_is_sorted(xmlformat));

	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

error:
	return FALSE;
}

osync_bool demerge_xmlformat(char **buf, unsigned int *size, OSyncCapabilities *caps, void *userdata, OSyncError **error)
{
	OSyncXMLFormat *xmlformat;
	OSyncXMLField *cur_xmlfield, *tmp;
	OSyncCapability *cur_capability;
	int rc;
	
	osync_trace(TRACE_ENTRY, "%s(%p, %p:%u, %p, %p)", __func__, buf, size, *size, caps, error);

	osync_assert(*size == osync_xmlformat_size());

	xmlformat = (OSyncXMLFormat *) *buf; 
	
	
        OSyncCapabilitiesObjType *capsobjtype = osync_capabilities_get_objtype(caps, osync_xmlformat_get_objtype(xmlformat));
        OSyncList *caplist = osync_capabilities_objtype_get_caps(capsobjtype);
	cur_capability = (OSyncCapability *) caplist->data;
	cur_xmlfield = osync_xmlformat_get_first_field(xmlformat);
	
	if(!cur_capability) /* if there is no capability - it means that the device can handle all xmlfields */
		goto end;

	while(cur_xmlfield != NULL)
		{
			if(cur_capability == NULL) {
				/* delete all xmlfields */
				while(cur_xmlfield) {
					osync_trace(TRACE_INTERNAL, "Demerge XMLField: %s", osync_xmlfield_get_name(cur_xmlfield));
					tmp = osync_xmlfield_get_next(cur_xmlfield);
					osync_xmlfield_delete(cur_xmlfield);
					cur_xmlfield = tmp;
				}
				break; 			
			}
		
			rc = strcmp(osync_xmlfield_get_name(cur_xmlfield), osync_capability_get_name(cur_capability));
			if(rc == 0) {
				/* check the secound level here */
				if(osync_capability_get_childs(cur_capability)) /* if there is no key - it means that the xmlfield can handle all keys */
					{
						int j=0;
						int capability_keys = osync_list_length(osync_capability_get_childs(cur_capability));
						//int xmlfield_keys = osync_xmlfield_get_key_count(cur_xmlfield);
	 			
						OSyncXMLField *child_xmlfield = osync_xmlfield_get_child(cur_xmlfield);

						for(; child_xmlfield; child_xmlfield = osync_xmlfield_get_next(child_xmlfield)) {
							int krc = 0;
							if(j == capability_keys) {
								for (; child_xmlfield; child_xmlfield = osync_xmlfield_get_next(child_xmlfield)) {
									osync_trace(TRACE_INTERNAL, "Demerge XMLField Key: %s",	osync_xmlfield_get_name(cur_xmlfield));
									osync_xmlfield_delete(child_xmlfield);
								}
								break;
							}

	 				
								OSyncCapability *child_cap = osync_list_nth_data(osync_capability_get_childs(cur_capability), j);
								krc = strcmp(osync_xmlfield_get_name(child_xmlfield), osync_capability_get_name(child_cap));

								if(krc == 0) {
									continue;
								}	
								if(krc > 0) {
									j++;
									continue;
								}
								if(krc < 0) {
									osync_trace(TRACE_INTERNAL, "Demerge XMLField Key: %s",	osync_xmlfield_get_name(child_xmlfield));
									osync_xmlfield_delete(child_xmlfield);
									continue;
								}
								g_assert_not_reached();
							}
					}
				cur_xmlfield = osync_xmlfield_get_next(cur_xmlfield);
				continue;
			}
			if(rc > 0) {
                                caplist = caplist->next;
				cur_capability = caplist->data;
				continue;
			}
			if(rc < 0) {
				/* delete xmlfield */
				osync_trace(TRACE_INTERNAL, "Demerge XMLField: %s", osync_xmlfield_get_name(cur_xmlfield));
				tmp = osync_xmlfield_get_next(cur_xmlfield);
				osync_xmlfield_delete(cur_xmlfield);
				cur_xmlfield = tmp;
				continue;
			}
			g_assert_not_reached();
			osync_error_set(error, OSYNC_ERROR_GENERIC, "Unexpected error during XMLFormat de-merging");
			goto error;
		}

 end:
	osync_trace(TRACE_EXIT, "%s", __func__);
	return TRUE;

error:
	osync_trace(TRACE_EXIT_ERROR, "%s: %s", __func__, osync_error_print(error));
	return FALSE;
}

