#include "support.h"

char *olddir = NULL;

char *setup_testbed(const char *fkt_name)
{
#ifndef _WIN32	
	setuid(65534);
#endif
	char *testbed = g_strdup_printf("%s/testbed.XXXXXX", g_get_tmp_dir());
	char *command = NULL, *dirname = NULL;
#ifdef _WIN32
        if(g_file_test(testbed, G_FILE_TEST_IS_DIR))
          destroy_testbed(g_strdup(testbed));

        if(g_mkdir(testbed,0777) < 0){
          osync_trace(TRACE_INTERNAL, "%s: Cannot create testbed directory %s", __func__, testbed);
          abort();
        }
#else /* WIN32 */
	if (!mkdtemp(testbed))
		abort();
#endif /* WIN32 */
	
	if (fkt_name) {
		dirname = g_strdup_printf(XMLFORMAT_TESTDATA"/%s", fkt_name);
		if (!g_file_test(dirname, G_FILE_TEST_IS_DIR)) {
			osync_trace(TRACE_INTERNAL, "%s: Path %s not exist.", __func__, dirname);
			abort();
		}
		command = g_strdup_printf("cp -R %s/* %s", dirname, testbed);
		if (system(command))
			abort();
		g_free(command);
		g_free(dirname);
	}
	
	command = g_strdup_printf("cp -R ../src/*.%s %s/", G_MODULE_SUFFIX, testbed);
	if (system(command))
		abort();
	g_free(command);

	command = g_strdup_printf("cp -R %s/../../schemas/*.xsd %s", XMLFORMAT_TESTDATA, testbed);
	if (system(command))
		abort();
	g_free(command);

#ifndef _WIN32	/* chmod is useless on windows system */
        command = g_strdup_printf("chmod -R 700 %s", testbed);
	if (system(command))
		abort();
	g_free(command);
#endif
		
	olddir = g_get_current_dir();
	if (g_chdir(testbed) < 0)
		abort();
	
	osync_trace(TRACE_INTERNAL, "Seting up %s at %s", fkt_name, testbed);
	return testbed;
}

void destroy_testbed(char *path)
{
	char *command = g_strdup_printf("rm -rf %s", path);
	if (olddir) {
		if (g_chdir(olddir) < 0)
			abort();
		g_free(olddir);
	}
	if (system(command))
		abort();

	g_free(command);
	osync_trace(TRACE_INTERNAL, "Tearing down %s", path);
	g_free(path);
}

void create_case(Suite *s, const char *name, TFun function)
{
	TCase *tc_new = tcase_create(name);
	tcase_set_timeout(tc_new, 30);
	suite_add_tcase (s, tc_new);
	tcase_add_test(tc_new, function);
}

