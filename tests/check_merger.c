#include "support.h"

#include <opensync/opensync-capabilities.h>
#include <opensync/opensync-serializer.h>
#include <opensync/opensync-xmlformat.h>


static OSyncCapabilities *_merger_assemble_caps(void)
{
	OSyncError *error;
	OSyncCapability *cap, *child_cap;

	OSyncCapabilities *capabilities = osync_capabilities_new("xmlformat-caps", &error);
	fail_unless(capabilities != NULL, NULL);

	/* Assemble Capabilities */
	OSyncCapabilitiesObjType *capsobjtype = osync_capabilities_add_new_objtype(capabilities, "contact", &error);
	fail_unless(capsobjtype != NULL, NULL);

	// Address, and children
	cap = osync_capabilities_add_new_capability(capsobjtype, &error);
	osync_capability_set_name(cap, "Address");

	child_cap = osync_capability_new_child(cap, &error);
	osync_capability_set_name(child_cap, "City");

	child_cap = osync_capability_new_child(cap, &error);
	osync_capability_set_name(child_cap, "Region");

	child_cap = osync_capability_new_child(cap, &error);
	osync_capability_set_name(child_cap, "Street");

	// Categories
	cap = osync_capabilities_add_new_capability(capsobjtype, &error);
	osync_capability_set_name(cap, "Categories");

	// Name, and children
	cap = osync_capabilities_add_new_capability(capsobjtype, &error);
	osync_capability_set_name(cap, "Name");

	child_cap = osync_capability_new_child(cap, &error);
	osync_capability_set_name(child_cap, "FirstName");

	child_cap = osync_capability_new_child(cap, &error);
	osync_capability_set_name(child_cap, "LastName");

	// Telephone
	cap = osync_capabilities_add_new_capability(capsobjtype, &error);
	osync_capability_set_name(cap, "Telephone");


	return capabilities;
}

START_TEST (merger_merge)
{
	char *testbed = setup_testbed("merger");

	char *buffer;
	unsigned int size;
	unsigned int xmlformat_size = osync_xmlformat_size();
	OSyncError *error = NULL;
	OSyncXMLFormat *xmlformat, *xmlformat_entire;
	OSyncCapabilities *capabilities = _merger_assemble_caps();

	fail_unless(osync_file_read("contact.xml", &buffer, &size, &error), NULL);
	xmlformat = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer);
	osync_xmlformat_sort(xmlformat, &error);
	fail_unless(error == NULL, NULL);
	
	fail_unless(osync_file_read("contact-full.xml", &buffer, &size, &error), NULL);
	xmlformat_entire = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat_entire != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer);
	osync_xmlformat_sort(xmlformat_entire, &error);
	fail_unless(error == NULL, NULL);

#if 0
	fail_unless(osync_file_read("capabilities.xml", &buffer, &size, &error), NULL);
	capabilities = osync_capabilities_parse(buffer, size, &error);
	fail_unless(capabilities != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer);
#endif
	
	fail_unless(merge_xmlformat((char **) &xmlformat, &xmlformat_size, (char *) xmlformat_entire, osync_xmlformat_size(), capabilities, NULL, &error), NULL);
	
	osync_capabilities_unref(capabilities);
	osync_xmlformat_unref(xmlformat);
	osync_xmlformat_unref(xmlformat_entire);
	
	destroy_testbed(testbed);
}
END_TEST

START_TEST (merger_demerge)
{
	char *testbed = setup_testbed("merger");

	char *buffer;
	unsigned int size;
	unsigned int xmlformat_size = osync_xmlformat_size();
	OSyncError *error = NULL;
	OSyncXMLFormat *xmlformat, *xmlformat_entire;
	OSyncCapabilities *capabilities = _merger_assemble_caps();

        OSyncCapabilitiesObjType *capsobjtype = osync_capabilities_get_objtype(capabilities, "contact");
        OSyncList *caplist = osync_capabilities_objtype_get_caps(capsobjtype);
	for (; caplist; caplist = caplist->next) {
		//OSyncCapability *cap_cur = (OSyncCapability *) caplist->data;
		//printf("Capability: %s\n", osync_capability_get_name(cap_cur));
	}

	fail_unless(osync_file_read("contact.xml", &buffer, &size, &error), NULL);
	xmlformat = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer);
	osync_xmlformat_sort(xmlformat, &error);
	fail_unless(error == NULL, NULL);
	
	fail_unless(osync_file_read("contact-full.xml", &buffer, &size, &error), NULL);
	xmlformat_entire = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat_entire != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer);
	osync_xmlformat_sort(xmlformat_entire, &error);
	fail_unless(error == NULL, NULL);

//osync_xmlformat_assemble(xmlformat, &buffer, &size, NULL); printf("XMLFORMAT:\n%s", buffer); g_free(buffer);
//osync_xmlformat_assemble(xmlformat_entire, &buffer, &size, NULL); printf("XMLFORMAT_ENTIRE\n%s", buffer); g_free(buffer);
	
	fail_unless(merge_xmlformat((char **) &xmlformat, &xmlformat_size, (char *) xmlformat_entire, osync_xmlformat_size(), capabilities, NULL, &error), NULL);
//osync_xmlformat_assemble(xmlformat, &buffer, &size, NULL); printf("\nMERGED:\n%s", buffer); g_free(buffer);
	fail_unless(demerge_xmlformat((char **) &xmlformat_entire, &xmlformat_size, capabilities, NULL, &error), NULL);
//osync_xmlformat_assemble(xmlformat_entire, &buffer, &size, NULL); printf("\nDEMERGED:\n%s", buffer); g_free(buffer);

	osync_capabilities_unref(capabilities);
	osync_xmlformat_unref(xmlformat);
	osync_xmlformat_unref(xmlformat_entire);
	
	destroy_testbed(testbed);
}
END_TEST

Suite *filter_suite(void)
{
	Suite *s = suite_create("Merger");
	create_case(s, "merger_merge", merger_merge);
	create_case(s, "merger_demerge", merger_demerge);
	return s;
}

int main(void)
{
	int nf;

	Suite *s = filter_suite();
	
	SRunner *sr;
	sr = srunner_create(s);
	srunner_run_all(sr, CK_VERBOSE);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
