#include "support.h"

START_TEST (xmlformat_init)
{
	char *testbed = setup_testbed("xmlformats");

	OSyncError *error = NULL;
	OSyncFormatEnv *formatenv = osync_format_env_new(&error); 
	fail_unless(formatenv != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(osync_format_env_load_plugins(formatenv, testbed, &error), NULL);
	fail_unless(error == NULL, NULL);

	osync_format_env_unref(formatenv);

	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_print)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer, *print_buffer;
	unsigned int size;
	OSyncError *error = NULL;

	fail_unless(osync_file_read( "contact.xml", &buffer, &size, &error), NULL);

	OSyncXMLFormat *xmlformat = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless((print_buffer = print_xmlformat((char *)xmlformat, osync_xmlformat_size(), NULL, &error)) != NULL, NULL);
	fail_unless(error == NULL, NULL);

	osync_free(print_buffer);

	osync_xmlformat_unref(xmlformat);

	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_destroy)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer;
	unsigned int size;
	OSyncError *error = NULL;

	fail_unless(osync_file_read( "contact.xml", &buffer, &size, &error), NULL);

	OSyncXMLFormat *xmlformat = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(destroy_xmlformat((char *)xmlformat, osync_xmlformat_size(), NULL, &error), NULL);
	fail_unless(error == NULL, NULL);

	osync_xmlformat_unref(xmlformat);

	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_compare_test)
{
	char *testbed = setup_testbed("xmlformats");

	OSyncConvCmpResult result1, result2;

	char *buffer;
	unsigned int size;
	OSyncError *error = NULL;


	fail_unless(osync_file_read( "contact.xml", &buffer, &size, &error), NULL);

	OSyncXMLFormat *xmlformat = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat != NULL, NULL);
	fail_unless(error == NULL, NULL);

	OSyncXMLFormat *xmlformat2 = osync_xmlformat_parse(buffer, size, &error);
	fail_unless(xmlformat2 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	g_free(buffer);

        char* keys_content[] =  {"Content", NULL};
        char* keys_name[] = {"FirstName", "LastName", NULL};
        OSyncXMLPoints points[] = {
                {"EMail",               10,     keys_content},
                {"Name",                90,     keys_name},
                {"Telephone",   10,     keys_content},
                {NULL}
        };


        result1 = xmlformat_compare((OSyncXMLFormat*)xmlformat, (OSyncXMLFormat*)xmlformat2, points, 0, 100);

	// Check for left-right / right-left compare issues
        result2 = xmlformat_compare((OSyncXMLFormat*)xmlformat2, (OSyncXMLFormat*)xmlformat, points, 0, 100);
	fail_unless(result1 == result2);

	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat);
	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat2);


	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_compare_field2null)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer1;
	char *buffer2;
	unsigned int size1;
	unsigned int size2;
	OSyncError *error = NULL;


	fail_unless(osync_file_read( "contact1.xml", &buffer1, &size1, &error), NULL);

	OSyncXMLFormat *xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(osync_file_read( "contact2.xml", &buffer2, &size2, &error), NULL);
	
	OSyncXMLFormat *xmlformat2 = osync_xmlformat_parse(buffer2, size2, &error);
	fail_unless(xmlformat2 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	g_free(buffer1);
	g_free(buffer2);

        char* keys_content[] =  {"Content", NULL};
        char* keys_name[] = {"FirstName", "LastName", NULL};
        OSyncXMLPoints points[] = {
                {"EMail",               10,     keys_content},
                {"Name",                90,     keys_name},
                {"Telephone",   10,     keys_content},
                {NULL}
        };

        xmlformat_compare((OSyncXMLFormat*)xmlformat1, (OSyncXMLFormat*)xmlformat2, points, 0, 100);


	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);
	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat2);


	destroy_testbed(testbed);
}
END_TEST


START_TEST (xmlformat_compare_ignore_fields)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer1;
	char *buffer2;
	unsigned int size1;
	unsigned int size2;
	OSyncError *error = NULL;
	OSyncConvCmpResult result;


	fail_unless(osync_file_read( "contact3_unique.xml", &buffer1, &size1, &error), NULL);

	OSyncXMLFormat *xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(osync_file_read( "contact3.xml", &buffer2, &size2, &error), NULL);
	
	OSyncXMLFormat *xmlformat2 = osync_xmlformat_parse(buffer2, size2, &error);
	fail_unless(xmlformat2 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	g_free(buffer1);
	g_free(buffer2);

        char* keys_content[] =  {"Content", NULL};
	char* keys_name[] = {"FirstName", "LastName", NULL};
        OSyncXMLPoints points[] = {
                {"EMail",               10,     keys_content},
                {"Name",                90,     keys_name},
                {"Revision",            -1,     keys_content},
                {"Telephone",           10,     keys_content},
                {"Uid",                 -1,     keys_content},
                {NULL}
        };

        result = xmlformat_compare((OSyncXMLFormat*)xmlformat1, (OSyncXMLFormat*)xmlformat2, points, 0, 100);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);

        result = xmlformat_compare((OSyncXMLFormat*)xmlformat2, (OSyncXMLFormat*)xmlformat1, points, 0, 100);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);


	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);
	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat2);

	/* Testcase: ignored fields on both side shouldn't change the compare result. */

	fail_unless(osync_file_read( "contact4_unique.xml", &buffer1, &size1, &error), NULL);

	xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(osync_file_read( "contact4.xml", &buffer2, &size2, &error), NULL);
	
	xmlformat2 = osync_xmlformat_parse(buffer2, size2, &error);
	fail_unless(xmlformat2 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	g_free(buffer1);
	g_free(buffer2);

        result = xmlformat_compare((OSyncXMLFormat*)xmlformat1, (OSyncXMLFormat*)xmlformat2, points, 0, 100);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);

        result = xmlformat_compare((OSyncXMLFormat*)xmlformat2, (OSyncXMLFormat*)xmlformat1, points, 0, 100);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);


	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);
	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat2);


	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_compare_ticket1021)
{
	char *testbed = setup_testbed("compare");

	char *buffer1;
	unsigned int size1;
	OSyncError *error = NULL;
	OSyncConvCmpResult result;


	fail_unless(osync_file_read( "contact1.xml", &buffer1, &size1, &error), NULL);

	OSyncXMLFormat *xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	OSyncXMLFormat *xmlformat2 = osync_xmlformat_new("contact", &error);
	fail_unless(xmlformat2 != NULL, NULL);
	fail_unless(error == NULL, NULL);

	OSyncXMLField *name = osync_xmlfield_new(xmlformat2, "Name", &error); 
	fail_unless(name != NULL, NULL);
	fail_unless(error == NULL, NULL);

	fail_unless(osync_xmlfield_set_key_value(name, "LastName", "llllllllll", &error), NULL);
	fail_unless(error == NULL, NULL);
	fail_unless(osync_xmlfield_set_key_value(name, "FirstName", "ffffffffff", &error), NULL);
	fail_unless(error == NULL, NULL);

	OSyncXMLField *tel = osync_xmlfield_new(xmlformat2, "Telephone", &error);
	fail_unless(osync_xmlfield_set_key_value(tel, "Content", "hhhhhhhhhh", &error), NULL);
	fail_unless(error == NULL, NULL);
	osync_xmlfield_set_attr(tel, "Location", "Home");

	g_free(buffer1);

        result = compare_contact((char*)xmlformat1, osync_xmlformat_size(),
			(char*)xmlformat2, osync_xmlformat_size(), NULL, &error);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);
	fail_unless(error == NULL, NULL);

        result = compare_contact((char*)xmlformat2, osync_xmlformat_size(),
			(char*)xmlformat1, osync_xmlformat_size(), NULL, &error);
	fail_unless(result == OSYNC_CONV_DATA_SAME, NULL);
	fail_unless(error == NULL, NULL);


	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);
	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat2);

	destroy_testbed(testbed);
}
END_TEST


START_TEST (xmlformat_marshal)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer1;
	unsigned int size1;
	OSyncError *error = NULL;

	fail_unless(osync_file_read( "contact1.xml", &buffer1, &size1, &error), NULL);

	OSyncXMLFormat *xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer1);

	OSyncMarshal *marshal = osync_marshal_new(&error);
	fail_unless(marshal != NULL, "Unexpected.");
	fail_unless(error == NULL, "Unexpected.");

	fail_unless(marshal_xmlformat((char *) xmlformat1, osync_xmlformat_size(), marshal, NULL, &error), NULL);

	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);
	osync_marshal_unref(marshal);


	destroy_testbed(testbed);
}
END_TEST

START_TEST (xmlformat_demarshal)
{
	char *testbed = setup_testbed("xmlformats");

	char *buffer1;
	unsigned int size1;
	OSyncError *error = NULL;

	fail_unless(osync_file_read( "contact1.xml", &buffer1, &size1, &error), NULL);

	OSyncXMLFormat *xmlformat1 = osync_xmlformat_parse(buffer1, size1, &error);
	fail_unless(xmlformat1 != NULL, NULL);
	fail_unless(error == NULL, NULL);
	g_free(buffer1);

	OSyncMarshal *marshal = osync_marshal_new(&error);
	fail_unless(marshal != NULL, "Unexpected.");
	fail_unless(error == NULL, "Unexpected.");

	fail_unless(marshal_xmlformat((char *) xmlformat1, osync_xmlformat_size(), marshal, NULL, &error), NULL);

	osync_xmlformat_unref((OSyncXMLFormat*)xmlformat1);

	fail_unless(demarshal_xmlformat(marshal, (char **)&xmlformat1, &size1, NULL, &error), NULL);

	osync_marshal_unref(marshal);

	destroy_testbed(testbed);
}
END_TEST

Suite *xmlformat_suite(void)
{
	Suite *s = suite_create("XMLFormat");
//	Suite *s2 = suite_create("XMLFormat");

	create_case(s, "xmlformat_init", xmlformat_init);
	create_case(s, "xmlformat_print", xmlformat_print);
	create_case(s, "xmlformat_destroy", xmlformat_destroy);
	create_case(s, "xmlformat_compare_test", xmlformat_compare_test);
	create_case(s, "xmlformat_compare_field2null", xmlformat_compare_field2null);
	create_case(s, "xmlformat_compare_ignore_fields", xmlformat_compare_ignore_fields);
	create_case(s, "xmlformat_compare_ticket1021", xmlformat_compare_ticket1021);
	create_case(s, "xmlformat_marshal", xmlformat_marshal);
	create_case(s, "xmlformat_demarshal", xmlformat_demarshal);

	return s;
}

int main(void)
{
	int nf;

	Suite *s = xmlformat_suite();
	
	SRunner *sr;
	sr = srunner_create(s);
	srunner_run_all(sr, CK_VERBOSE);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
