#include "config.h"

#include <check.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <opensync/opensync.h>
#include <opensync/opensync-xmlformat.h>
#include "src/xmlformat.h"


char *setup_testbed(const char *fkt_name);
void destroy_testbed(char *path);
void create_case(Suite *s, const char *name, TFun function);

